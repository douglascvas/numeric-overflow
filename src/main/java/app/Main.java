package app;

import java.math.BigDecimal;

public class Main {

    private final BigDecimal threshold = BigDecimal.valueOf(1000);

    public static void main(String[] args) {

        Main app = new Main();

        Amount amount = new Amount(BigDecimal.valueOf(999));
        if (!app.approval(amount)) {
            System.out.println(amount + " does not require approval");
        }
        amount = new Amount(BigDecimal.valueOf(2000));
        if (app.approval(amount)) {
            System.out.println(amount + " requires approval");
        }
    }

    public boolean approval(Amount amount) {
        if (amount.getValue().compareTo(threshold) >= 0) {
            return true;
        }
        return false;
    }

    public static class Amount {
        private final BigDecimal value;

        public Amount(BigDecimal value) {
            if (value.compareTo(BigDecimal.valueOf(Integer.MIN_VALUE)) < 0) {
                throw new RuntimeException("Invalid amount");
            }
            if (value.compareTo(BigDecimal.valueOf(Integer.MAX_VALUE)) > 0) {
                throw new RuntimeException("Invalid amount");
            }
            this.value = value;
        }

        public BigDecimal getValue() {
            return value;
        }
    }

}
