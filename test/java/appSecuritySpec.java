package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@DisplayName("Security unit tests")
@Tag("security")
public class appSecuritySpec {

    @Test
    public void biggerThanIntMaxSizeShouldFail() {
        try {
            new Main.Amount(BigDecimal.valueOf(2147483647).add(BigDecimal.ONE));
            fail("Should throw exception");
        } catch (Exception e) {
            assertEquals(e.getMessage(), "Invalid amount");
        }
    }

    @Test
    public void withinIntValidSizeShouldPass() {
        new Main.Amount(BigDecimal.valueOf(2147483646).add(BigDecimal.ONE));
    }

    @Test
    public void lessThanIntMinSizeShouldFail() {
        try {
            new Main.Amount(BigDecimal.valueOf(-2147483648).subtract(BigDecimal.ONE));
            fail("Should throw exception");
        } catch (Exception e) {
            assertEquals(e.getMessage(), "Invalid amount");
        }
    }
}
