= Numeric overflow

//tag::abstract[]

Numeric overflow in Java is commonly overlooked. 
This vulnerability appears where the range of
numeric is not being validated.

//end::abstract[]

For example, 
Integer in Java is 32 bits, therefore it ranges
between -/+2,147,483,647.

If a given value is bigger or smaller than Integer
range, the variable overflows and turn to a negative
or positive numbers respectively.

This vulnerability can cause logic bypass often
result in serious impact. Think how in a banking
application a daily transaction threshold can get
passed by using this vulnerability?

This vulnerabilities affects all numeric types
that do no use dynamic memory allocation.

//tag::lab[]

== Lab

Your objective in each lab is to pass the build.


=== Task 0

Fork and clone https://gitlab.com/insecure-programming/java/integer-overflow[this repository].
Install docker, git and make on your system.

. Build the program: `make build`.
. Run it: `make run`.
. Run unit tests: `make test`. 
. Run security tests: `make securitytest`.

Note: The last test will fail. 

=== Task 1

Review the program code (`src/main/java/app/Main.java`) 
try to find out why security tests fails. 

Note: Avoid looking at tests or patch file and try to
spot the vulnerable pattern on your own.

=== Task 2

The amount variable is vulnerable to Integer Overflow.
Write some lines of code to verify the range
and discard unreasonably big or small values.  
Run the security tests. Do they pass now?

=== Task 3

Review `test/java/appSecuritySpec.java` and see how security tests
works. Review your patch from Task 2.
Make sure this time the security tests pass.
If you stuck, move to the next task.

=== Task 4

Check out the `patch` branch and review the program code.
Run all tests and make sure everything pass.

=== Task 5

Merge the patch branch to master.
Commit and push your changes.
Does pipeline show that you have passed the build? 

//end::lab[]

//tag::references[]

== References

* https://wiki.sei.cmu.edu/confluence/display/java/NUM00-J.+Detect+or+prevent+integer+overflow[NUM00-J. Detect or prevent integer overflow]

//end::references[]
